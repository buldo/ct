﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buldo.CT.Common.Helpers
{
    public static class Arrays
    {
        public static float[,] StupidArrayCopy(float[,] array)
        {
            var a = array.GetLength(0);
            var b = array.GetLength(1);

            var retArr = new float[array.GetLength(0), array.GetLength(1)];
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < b; j++)
                {
                    retArr[i, j] = array[i, j];
                }
            }
            return retArr;
        }

        /// <summary>
        /// Returns float matrix filled by initValues
        /// </summary>
        /// <param name="width">
        /// The width.
        /// </param>
        /// <param name="height">
        /// The height.
        /// </param>
        /// <param name="initValue">
        /// The init value.
        /// </param>
        /// <returns>
        /// The <see cref="float"/>.
        /// </returns>
        public static float[,] GetFilledArray(int width, int height, float initValue = default(float))
        {
            var ret = new float[height, width];
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    ret[i, j] = initValue;
                }
            }

            return ret;
        }
    }
}
