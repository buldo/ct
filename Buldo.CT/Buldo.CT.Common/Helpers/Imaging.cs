﻿namespace Buldo.CT.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Helper for working with images
    /// </summary>
    public static class Imaging
    {
        /// <summary>
        /// Gets grayscale bitmap from float array
        /// </summary>
        /// <param name="imageArray">
        /// The image array.
        /// </param>
        /// <returns>
        /// The <see cref="WriteableBitmap"/>.
        /// </returns>
        public static WriteableBitmap GetBitmapFromArray(float[,] imageArray)
        {
            var width = imageArray.GetLength(1);
            var height = imageArray.GetLength(0);

            var min = float.MaxValue;
            var max = float.MinValue;

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (float.IsNaN(imageArray[i, j]))
                    {
                        continue;
                    }
                    min = Math.Min(min, imageArray[i, j]);
                    max = Math.Max(max, imageArray[i, j]);
                }
            }

            var multCorrection = max - min;

            var bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Gray32Float, null);
            var pixels = new List<byte>();
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    pixels.AddRange(BitConverter.GetBytes((imageArray[i, j] - min) / multCorrection));
                }
            }

            bitmap.WritePixels(
                new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight),
                pixels.ToArray(),
                width * sizeof(float),
                0);

            return bitmap;
        }
    }
}
