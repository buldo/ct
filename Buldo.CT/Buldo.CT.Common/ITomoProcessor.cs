﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buldo.CT.Common
{
    public interface ITomoProcessor
    {
        float[,] GetNewImage(Sinogram sinogram, float[,] oldImage, int iterations = 1);

        Task<float[,]> GetNewImageAsync(Sinogram sinogram, float[,] oldImage, int iterations = 1);
    }
}
