﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buldo.CT.Common
{
    using System.Xml.Schema;

    public class Sinogram
    {
        public Sinogram(List<List<float>> values, float stepDeg)
        {
            // Скопирую значения из входных данных
            Values = new List<List<float>>();
            foreach (var list in values)
            {
                Values.Add(new List<float>(list));
            }

            Width = Values[0].Count;
            HalfWidth = Width / 2;
            StepAngle = stepDeg;
            Steps = Values.Count;
        }

        public List<List<float>> Values { get; private set; }

        public int Steps { get; private set; }

        /// <summary>
        /// Получает угол поворота проекции в радианах
        /// </summary>
        public float StepAngle { get; private set; }

        public int Width { get; }

        public float HalfWidth { get; private set; }

        public float[,] GetValuesArray()
        {
            var imageArray = new float[Values.Count, Values.First().Count];

            for (int i = 0; i < Values.Count; i++)
            {
                var row = Values[i];
                for (int j = 0; j < row.Count; j++)
                {
                    imageArray[i, j] = Values[i][j];
                }
            }

            return imageArray;
        }
    }
}
