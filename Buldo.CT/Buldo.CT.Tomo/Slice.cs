﻿using Buldo.CT.IO;

namespace Buldo.CT.Tomo
{
    using System;
    using Common;
    using Common.Helpers;
    
    /// <summary>
    /// Класс, описывающий сечение 
    /// </summary>
    public class Slice
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Slice"/> class.
        /// </summary>
        /// <param name="sinogram">
        /// Синограмма данного сечения
        /// </param>
        public Slice(Sinogram sinogram)
        {
            Sinogram = sinogram;
            RestoredImage = Arrays.GetFilledArray(Sinogram.Width, Sinogram.Width, 1f);
        }

        /// <summary>
        /// The new image ready.
        /// </summary>
        public event EventHandler<EventArgs> NewImageReady;

        /// <summary>
        /// Gets the sinogram.
        /// </summary>
        public Sinogram Sinogram { get; }

        /// <summary>
        /// Gets the restored image.
        /// </summary>
        public float[,] RestoredImage { get; private set; }

        /// <summary>
        /// The compute next iterations.
        /// </summary>
        /// <param name="processor">
        /// The processor.
        /// </param>
        /// <param name="iterations">
        /// The iterations.
        /// </param>
        async public void ComputeNextIterations(ITomoProcessor processor, int iterations = 1)
        {
            var newImage = processor.GetNewImageAsync(Sinogram, RestoredImage, iterations);
            RestoredImage = await newImage;
            OnNewImageReady();
        }

        /// <summary>
        /// The on new image ready.
        /// </summary>
        protected virtual void OnNewImageReady()
        {
            NewImageReady?.Invoke(this, EventArgs.Empty);
        }

        public void SaveToFile(string filename)
        {
            Tiff.WriteArrayToFile(filename, RestoredImage);
        }
    }
}
