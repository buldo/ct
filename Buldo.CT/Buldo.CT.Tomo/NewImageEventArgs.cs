﻿namespace Buldo.CT.Tomo
{
    using System;

    /// <summary>
    /// The new image event args.
    /// </summary>
    public class NewImageEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewImageEventArgs"/> class.
        /// </summary>
        /// <param name="image">
        /// The image.
        /// </param>
        /// <param name="projectionIndex">
        /// The projection index.
        /// </param>
        public NewImageEventArgs(float[,] image, int projectionIndex)
        {
            Image = image;
            ProjectionIndex = projectionIndex;
        }

        /// <summary>
        /// Gets the projection index.
        /// </summary>
        public int ProjectionIndex { get; private set; }

        /// <summary>
        /// Gets the image.
        /// </summary>
        public float[,] Image { get; private set; }

    }
}
