__kernel void MakeBackProjection(
	__global float* oldImage,
    int imageVectorLen,
	__global float* res,
	float angleSin, 
	float angleCos,
    int sinoWidth,
	float sinoHalfWidth,
	int detectorSize, 
	__global  float* newImage)
{
    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int width = get_global_size(0);
    const int id = y * width + x;
       
    if(id >= imageVectorLen)
    {
        return;
    }
    
    float yCoord = (float)y - sinoHalfWidth;
    float xCoord = (float)x - sinoHalfWidth;
    
    float yx = yCoord * angleCos + xCoord * angleSin + sinoHalfWidth;
    float y1x = (yCoord + 1) * angleCos + xCoord * angleSin + sinoHalfWidth;
    float yx1 = yCoord * angleCos + (xCoord + 1) * angleSin + sinoHalfWidth;
    float y1x1 = (yCoord + 1) * angleCos + (xCoord + 1) * angleSin + sinoHalfWidth;
    
    int minBpx = max(0, (int)round(fmin(yx,fmin(y1x,fmin(yx1,y1x1)))));
    int maxBpx = min(sinoWidth, (int)round(fmax(yx,fmax(y1x,fmax(yx1,y1x1)))));
    
    float resSum = 0;
    int raysCnt = 0;
    for(int i = minBpx; i < maxBpx; i++)
    {
        resSum += res[i];
        raysCnt++;
    }
    
    newImage[id] = oldImage[id] + (float)0.25 * resSum / (float)raysCnt; 
}


