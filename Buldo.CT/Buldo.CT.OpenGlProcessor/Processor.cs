﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buldo.CT.Common;
using Buldo.CT.Common.Helpers;
using CtKernel;
using OpenCL.Net;
using OpenCL.Net.Extensions;

namespace Buldo.CT.OpenGlProcessor
{
    public class Processor : ITomoProcessor
    {
        public Task<float[,]> GetNewImageAsync(Sinogram sinogram, float[,] oldImage, int iterations = 1)
        {
            return Task.Run(() => GetNewImage(sinogram, oldImage, iterations));
        }

        public float[,] GetNewImage(Sinogram sinogram, float[,] oldImage, int iterations = 1)
        {
            var radius = (sinogram.Width / 2f) * 1.42f;
            var tempImage = Arrays.StupidArrayCopy(oldImage);
            for (int i = 0; i < iterations; i++)
            {
                for (var projectionIndex = 0; projectionIndex < sinogram.Steps; projectionIndex++)
                {
                    var projection = sinogram.Values[projectionIndex];

                    var rNorm = new List<float>();
                    for (var rayIndex = 0; rayIndex < sinogram.Width; rayIndex++)
                    {

                        var pixs = FindPixels(sinogram, radius, rayIndex, projectionIndex);
                        var sum = pixs.Sum(pix => oldImage[pix.Item2, pix.Item1]);
                        rNorm.Add(pixs.Count > 0 ? (projection[rayIndex] - sum) / pixs.Count : 0);
                    }


                    //tempImage = OpenClBackProjection()
                }
            }
            return tempImage;
        }

        private List<Tuple<int, int>> FindPixels(Sinogram sinogram, float radius, int rayId, int step)
        {
            var cells = new HashSet<Tuple<int, int>>();
            var angle = step * sinogram.StepAngle;
            var anglePlusHalhPi = step * sinogram.StepAngle + Math.PI / 2;
            var anglePlusPi = step * sinogram.StepAngle + Math.PI;
            var anglePlus23Pi = step * sinogram.StepAngle + 3 * Math.PI / 2;
            var centerX = radius * Math.Cos(angle);
            var centerY = radius * Math.Sin(angle);
            var rayStartX = Math.Cos(anglePlusHalhPi) * (sinogram.HalfWidth) + centerX;
            var rayStartY = Math.Sin(anglePlusHalhPi) * (sinogram.HalfWidth) + centerY;

            // Находим шаг смещения по X
            var stepX = Math.Cos(anglePlusPi);

            // Находим шаг смещения по Y
            var stepY = Math.Sin(anglePlusPi);

            var startX = Math.Cos(anglePlus23Pi) * (rayId + 0.5f) + rayStartX;
            var startY = Math.Sin(anglePlus23Pi) * (rayId + 0.5f) + rayStartY;
            var currentX = startX;
            var currentY = startY;
            while (currentX <= sinogram.Width &&
                   currentX >= -sinogram.Width &&
                   currentY <= sinogram.Width &&
                   currentY >= -sinogram.Width)
            {
                var curCell = new Tuple<int, int>((int)(currentX + sinogram.HalfWidth), (int)(-currentY + sinogram.HalfWidth));
                if (curCell.Item1 >= 0 && curCell.Item1 < sinogram.Width &&
                    curCell.Item2 >= 0 && curCell.Item2 < sinogram.Width)
                {
                    cells.Add(curCell);
                }

                currentX += stepX;
                currentY += stepY;
            }

            return cells.ToList();
        }

        private float[,] OpenClBackProjection(float[,] oldImage, float[] rNorm, int projectionIndex)
        {
            //var env = "*Intel*".CreateCLEnvironment();
            //var inImgArray = ToSingleArray(oldImage);
            //var inImg = env.Context.CreateBuffer(inImgArray, MemFlags.ReadOnly | MemFlags.UseHostPtr);
            //var outImgArray = new float[inImgArray.Length];
            //var outImg = env.Context.CreateBuffer(outImgArray, MemFlags.WriteOnly | MemFlags.UseHostPtr);


            //var kernel = new MakeBackProjection(env.Context);
            //kernel.Run(env.CommandQueues, inImg, inImgArray.Length, outImg,);
            ////var oldImageBuffer = env.Context.CreateBuffer()

            return new float[5, 5];
        }
    }
}
