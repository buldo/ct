﻿namespace Buldo.CT.ImageLogger
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Configuration of log system
    /// </summary>
    internal class LogConfiguration
    {
        /// <summary>
        /// List of log event receivers
        /// </summary>
        private readonly List<IImageLogReceiver> _receivers = new List<IImageLogReceiver>();

        /// <summary>
        /// Lock object for working with log receivers
        /// </summary>
        private readonly object _reveiversLockObject = new object();

        /// <summary>
        /// Gets or sets a value indicating whether is log enabled.
        /// </summary>
        public bool IsLogEnabled { get; set; }

        /// <summary>
        /// Gets the list of log receivers for current moment
        /// </summary>
        public ReadOnlyCollection<IImageLogReceiver> Receivers
        {
            get
            {
                ReadOnlyCollection<IImageLogReceiver> ret;
                lock (_reveiversLockObject)
                {
                    ret = new ReadOnlyCollection<IImageLogReceiver>(_receivers);
                }

                return ret;
            }
        }

        /// <summary>
        /// Add receiver to receivers list
        /// </summary>
        /// <param name="receiver">
        /// The receiver.
        /// </param>
        public void AddReceiver(IImageLogReceiver receiver)
        {
            lock (_reveiversLockObject)
            {
                if (!_receivers.Contains(receiver))
                {
                    _receivers.Add(receiver);
                }
            }
        }
    }
}
