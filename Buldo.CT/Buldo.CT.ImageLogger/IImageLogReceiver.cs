﻿namespace Buldo.CT.ImageLogger
{
    /// <summary>
    /// Interface for log event receivers
    /// </summary>
    public interface IImageLogReceiver
    {
        /// <summary>
        /// Process new event.
        /// </summary>
        /// <param name="logEvent">
        /// The new log event.
        /// </param>
        void ProcessNewEvent(ImageLogEvent logEvent);
    }
}
