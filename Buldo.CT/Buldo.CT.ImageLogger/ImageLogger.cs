﻿namespace Buldo.CT.ImageLogger
{
    /// <summary>
    /// The image logger.
    /// </summary>
    public class ImageLogger
    {
        /// <summary>
        /// Log system configuration
        /// </summary>
        private readonly LogConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageLogger"/> class.
        /// </summary>
        /// <param name="name">
        /// Name of new logger
        /// </param>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        internal ImageLogger(string name, LogConfiguration configuration)
        {
            _configuration = configuration;
            Name = name;
        }

        /// <summary>
        /// Gets the name of current logger
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The log the image.
        /// </summary>
        /// <param name="image">
        /// The image.
        /// </param>
        /// <param name="description">
        /// The description of the image
        /// </param>
        public void LogImage(float[,] image, string description = "")
        {
            if (_configuration.IsLogEnabled)
            {
                var newEvent = new ImageLogEvent(image, description, this);
                var recList = _configuration.Receivers;
                foreach (var receiver in recList)
                {
                    receiver.ProcessNewEvent(newEvent);
                }
            }
        }
    }
}
