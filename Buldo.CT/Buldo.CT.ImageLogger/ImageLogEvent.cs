﻿namespace Buldo.CT.ImageLogger
{
    /// <summary>
    /// Class that describes a log event
    /// </summary>
    public class ImageLogEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageLogEvent"/> class.
        /// </summary>
        /// <param name="image">
        /// The image.
        /// </param>
        /// <param name="descriprion">
        /// The descriprion.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public ImageLogEvent(float[,] image, string descriprion, ImageLogger logger)
        {
            Image = image;
            Description = descriprion;
            Logger = logger;
        }

        /// <summary>
        /// Gets the image.
        /// </summary>
        public float[,] Image { get; }

        /// <summary>
        /// Gets the description of current event
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Gets the logger thats generated this event
        /// </summary>
        public ImageLogger Logger { get; }
    }
}
