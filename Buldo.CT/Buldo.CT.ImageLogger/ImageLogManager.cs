﻿namespace Buldo.CT.ImageLogger
{
    /// <summary>
    /// The image log manager.
    /// </summary>
    public static class ImageLogManager
    {
        /// <summary>
        /// Configuration of log system
        /// </summary>
        private static LogConfiguration _configuration = new LogConfiguration();
        
        /// <summary>
        /// Gets or sets a value indicating whether is log enabled.
        /// </summary>
        public static bool IsLogEnabled
        {
            get { return _configuration.IsLogEnabled; }
            set { _configuration.IsLogEnabled = value; }
        }

        /// <summary>
        /// Add receiver to log system
        /// </summary>
        /// <param name="receiver">
        /// The receiver.
        /// </param>
        public static void RegisterReceiver(IImageLogReceiver receiver)
        {
            _configuration.AddReceiver(receiver);
        }
        
        /// <summary>
        /// Get a new Logger
        /// </summary>
        /// <param name="loggerName">
        /// The logger Name.
        /// </param>
        /// <returns>
        /// The <see cref="ImageLogger"/>.
        /// </returns>
        public static ImageLogger GetNewLogger(string loggerName)
        {
            return new ImageLogger(loggerName, _configuration);
        }
    }
}
