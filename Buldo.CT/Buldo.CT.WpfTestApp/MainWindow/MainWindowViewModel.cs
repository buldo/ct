﻿using System;
using System.Collections.Generic;
    using System.Collections.ObjectModel;
using System.Windows.Input;
using Buldo.CT.Common;
using Buldo.CT.Tomo;
using Buldo.CT.WpfTestApp.ViewModels;
    using Microsoft.Practices.Prism.Mvvm;
    using Microsoft.Practices.Prism.Commands;

namespace Buldo.CT.WpfTestApp.MainWindow
    {
    public class MainWindowViewModel : BindableBase
    {
        private SliceViewModel _selectedSlice;

        public MainWindowViewModel()
        {
            Slices = new ObservableCollection<SliceViewModel>();
            FillSlices();
            SaveCommand = new DelegateCommand(ExecuteSaveCommand);
        }

        public ObservableCollection<SliceViewModel> Slices { get; private set; }

        public SliceViewModel SelectedSlice
        {
            get
            {
                return _selectedSlice;
            }
            private set
            {
                SetProperty(ref _selectedSlice,value);
            }
        }

        public ICommand SaveCommand { get; private set; }

        private void FillSlices()
        {
            var forbild = new SliceViewModel(new Slice(IO.Tiff.ReadSinoFromFile(@"Data\forbild256_sino072.tif", (float)(0.72f * Math.PI / 180))), "Forbild");

            var blackSinoVal = new List<List<float>>();
            for (int i = 0; i < 200; i++)
            {
                var row = new List<float>();
                blackSinoVal.Add(row);
                for (int j = 0; j < 180; j++)
                {
                    row.Add(1f);
                }
            }
            var blackSino = new Sinogram(blackSinoVal, (float)(Math.PI / 180));
            var blackSinoSlice = new SliceViewModel(new Slice(blackSino), "Черная синограмма");

            //var rowSinoVal = new List<List<float>>();
            //for (int i = 0; i < 200; i++)
            //{
            //    var row = new List<float>();
            //    rowSinoVal.Add(row);
            //    for (int j = 0; j < 180; j++)
            //    {
            //        row.Add(i > 90 && i < 110 ? 1f : 0f);
            //    }
            //}
            //var rowSino = new Sinogram(rowSinoVal, (float)(Math.PI / 180));
            //var rowSinoSlice = new SliceViewModel(new Slice(rowSino), "Полоса");

            //Slices.Add(rowSinoSlice);
            Slices.Add(blackSinoSlice);
            Slices.Add(forbild);
        }


        private void ExecuteSaveCommand()
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Result"; // Default file name
            dlg.DefaultExt = ".tiff"; // Default file extension
            dlg.Filter = "Tiff documents (.tiff)|*.tiff"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                SelectedSlice.SaveToFile(filename);
            }
        }
    }
}
