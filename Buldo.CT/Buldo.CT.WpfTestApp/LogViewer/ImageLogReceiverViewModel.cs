﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Buldo.CT.ImageLogger;
using ObservableImmutable;

namespace Buldo.CT.WpfTestApp.LogViewer
{
    class ImageLogReceiverViewModel
    {
        /// <summary>
        /// The model of Image Log Receiver
        /// </summary>
        private readonly ImageLogReceiverModel _model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageLogReceiverViewModel"/> class.
        /// </summary>
        public ImageLogReceiverViewModel()
        {
            _model = new ImageLogReceiverModel();
            _model.NewImage += (sender, args) => ProcessNewImages();
            ProcessNewImages();
        }

        public ObservableImmutableList<LogEventViewModel> LogEvents { get; } = new ObservableImmutableList<LogEventViewModel>();

        private void ProcessNewImages()
        {
            ImageLogEvent newEvent;
            while ((newEvent = _model.GetEvent()) != null)
            {
                var logEvent = newEvent;
                Task.Run(() => LogEvents.Add(new LogEventViewModel(logEvent)));
            }
        }
    }
}
