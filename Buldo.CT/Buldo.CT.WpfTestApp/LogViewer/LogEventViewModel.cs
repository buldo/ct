﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buldo.CT.ImageLogger;

namespace Buldo.CT.WpfTestApp.LogViewer
{
    using System.Windows.Media.Imaging;
    using Buldo.CT.Common.Helpers;
    using Microsoft.Practices.Prism.Mvvm;

    internal class LogEventViewModel
    {
        public LogEventViewModel(ImageLogEvent logEvent)
        {
            Bitmap = Imaging.GetBitmapFromArray(logEvent.Image);
            Bitmap.Freeze();
            LoggerName = logEvent.Logger.Name;
            Description = logEvent.Description;
        }

        public WriteableBitmap Bitmap { get; }

        public string Description { get; }

        public string LoggerName { get; }
    }
}
