﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buldo.CT.ImageLogger;

namespace Buldo.CT.WpfTestApp.LogViewer
{
    class ImageLogReceiverModel : IImageLogReceiver
    {
        private ConcurrentQueue<ImageLogEvent> _storedEvents = new ConcurrentQueue<ImageLogEvent>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageLogReceiverModel"/> class.
        /// </summary>
        public ImageLogReceiverModel()
        {
            ImageLogManager.RegisterReceiver(this);
        }

        public ImageLogEvent GetEvent()
        {
            ImageLogEvent ev;
            _storedEvents.TryDequeue(out ev);
            return ev;
        }

        /// <summary>
        /// Processing of a newLogEvent
        /// </summary>
        /// <param name="logEvent">
        /// The log event.
        /// </param>
        void IImageLogReceiver.ProcessNewEvent(ImageLogEvent logEvent)
        {
            _storedEvents.Enqueue(logEvent);
            OnNewImage();
        }

        public event EventHandler<EventArgs> NewImage;

        protected virtual void OnNewImage()
        {
            NewImage?.Invoke(this, EventArgs.Empty);
        }
    }
}
