﻿using Buldo.CT.Common;

namespace Buldo.CT.WpfTestApp.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;
    using Buldo.CT.Tomo;

    using Buldo.CT.Common.Helpers;

    /// <summary>
    /// Модель представление для слайса
    /// </summary>
    public class SliceViewModel : BindableBase
    {
        /// <summary>
        /// Модель слайса
        /// </summary>
        private readonly Slice _slice;

        /// <summary>
        /// Bitmap for last restored image
        /// </summary>
        private WriteableBitmap _lastBitmap;

        /// <summary>
        /// Initializes a new instance of the <see cref="SliceViewModel"/> class.
        /// </summary>
        /// <param name="slice">
        /// The slice.
        /// </param>
        /// <param name="name">
        /// Name of the slice
        /// </param>
        public SliceViewModel(Slice slice, string name)
        {
            Name = name;
            _slice = slice;
            SinogramBitmap = Imaging.GetBitmapFromArray(_slice.Sinogram.GetValuesArray());
            _lastBitmap = Imaging.GetBitmapFromArray(_slice.RestoredImage);
            _slice.NewImageReady += OnNewImage;
            IterationsToExecute = 1;

            ExecuteNextIterationsCommand = new DelegateCommand(ExecuteNextIteration);
            AvailableProcessors.Add(new CpuProcessor.Processor());
            SelectedProcessor = AvailableProcessors.First();
        }

        public int IterationsToExecute { get; set; }

        public ITomoProcessor SelectedProcessor { get; set; }

        public ObservableCollection<ITomoProcessor> AvailableProcessors { get; } = new ObservableCollection<ITomoProcessor>();

        public WriteableBitmap SinogramBitmap { get; private set; }

        public WriteableBitmap LastBitmap
        {
            get
            {
                return _lastBitmap;
            }
            private set
            {
                SetProperty(ref _lastBitmap, value);
            }
        }
        
        public ICommand ExecuteNextIterationsCommand { get; }
        
        public string Name { get; private set; }

        public void ExecuteNextIteration()
        {
            _slice.ComputeNextIterations(SelectedProcessor, IterationsToExecute);
        }
        
        private void OnNewImage(object sender, EventArgs e)
        {
            LastBitmap = Imaging.GetBitmapFromArray(_slice.RestoredImage);
        }

        internal void SaveToFile(string filename)
        {
            _slice.SaveToFile(filename);
        }
    }
}
