﻿

namespace Buldo.CT.CpuProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Common;
    using Common.Helpers;
    using ImageLogger;

    /// <summary>
    /// The processor.
    /// </summary>
    public class Processor : ITomoProcessor
    {
        /// <summary>
        /// The image logger
        /// </summary>
        private ImageLogger _imageLogger = ImageLogManager.GetNewLogger("Cpu processor");

        /// <summary>
        /// The get new image.
        /// </summary>
        /// <param name="sinogram">
        /// The sinogram.
        /// </param>
        /// <param name="oldImage">
        /// The old image.
        /// </param>
        /// <param name="iterations">
        /// The iterations.
        /// </param>
        /// <returns>
        /// The <see cref="float[,]"/>.
        /// </returns>
        public float[,] GetNewImage(Sinogram sinogram, float[,] oldImage, int iterations = 1)
        {
            _imageLogger.LogImage(oldImage, "StartImage");

            var radius = (sinogram.Width / 2f) * 1.42f;
            var tempImage = Arrays.StupidArrayCopy(oldImage);
            for (int i = 0; i < iterations; i++)
            {
                for (var projectionIndex = 0; projectionIndex < sinogram.Steps; projectionIndex++)
                {
                    var projection = sinogram.Values[projectionIndex];

                    var rNorm = new List<float>();
                    for (var rayIndex = 0; rayIndex < sinogram.Width; rayIndex++)
                    {
                        var pixs = FindPixels(sinogram, radius, rayIndex, projectionIndex);
                        var sum = pixs.Sum(pix => tempImage[pix.Item2, pix.Item1]);
                        rNorm.Add(pixs.Count > 0 ? (projection[rayIndex] - sum) / pixs.Count : 0);
                    }
                    
                    for (var y = 0; y < sinogram.Width; y++)
                    {
                        for (var x = 0; x < sinogram.Width; x++)
                        {
                            var rays = FindRays(sinogram, x, y, projectionIndex);
                            if (rays.Count > 0)
                            {
                                tempImage[y, x] += 0.25f * rays.Select(ray => rNorm[ray]).Sum() / rays.Count;
                            }
                        }
                    }

                    _imageLogger.LogImage(tempImage);
                }
            }

            return tempImage;
        }

        /// <summary>
        /// The get new image async.
        /// </summary>
        /// <param name="sinogram">
        /// The sinogram.
        /// </param>
        /// <param name="oldImage">
        /// The old image.
        /// </param>
        /// <param name="iterations">
        /// The iterations.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<float[,]> GetNewImageAsync(Sinogram sinogram, float[,] oldImage, int iterations = 1)
        {
            return Task.Run(() => GetNewImage(sinogram, oldImage, iterations));
        }

        /// <summary>
        /// The find pixels.
        /// </summary>
        /// <param name="sinogram">
        /// The sinogram.
        /// </param>
        /// <param name="radius">
        /// The radius.
        /// </param>
        /// <param name="rayId">
        /// The ray id.
        /// </param>
        /// <param name="step">
        /// The step.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<Tuple<int, int>> FindPixels(Sinogram sinogram, float radius, int rayId, int step)
        {
            var cells = new HashSet<Tuple<int, int>>();
            var angle = step * sinogram.StepAngle;
            var anglePlusHalhPi = step * sinogram.StepAngle + Math.PI / 2;
            var anglePlusPi = step * sinogram.StepAngle + Math.PI;
            var anglePlus23Pi = step * sinogram.StepAngle + 3 * Math.PI / 2;
            var centerX = radius * Math.Cos(angle);
            var centerY = radius * Math.Sin(angle);
            var rayStartX = Math.Cos(anglePlusHalhPi) * sinogram.HalfWidth + centerX;
            var rayStartY = Math.Sin(anglePlusHalhPi) * sinogram.HalfWidth + centerY;

            // Находим шаг смещения по X
            var stepX = Math.Cos(anglePlusPi);

            // Находим шаг смещения по Y
            var stepY = Math.Sin(anglePlusPi);

            var startX = Math.Cos(anglePlus23Pi) * (rayId + 0.5f) + rayStartX;
            var startY = Math.Sin(anglePlus23Pi) * (rayId + 0.5f) + rayStartY;
            var currentX = startX;
            var currentY = startY;
            while (currentX <= sinogram.Width &&
                   currentX >= -sinogram.Width &&
                   currentY <= sinogram.Width &&
                   currentY >= -sinogram.Width)
            {
                var curCell = new Tuple<int, int>((int)(currentX + sinogram.HalfWidth), (int)(-currentY + sinogram.HalfWidth));
                if (curCell.Item1 >= 0 && curCell.Item1 < sinogram.Width &&
                    curCell.Item2 >= 0 && curCell.Item2 < sinogram.Width)
                {
                    cells.Add(curCell);
                }

                currentX += stepX;
                currentY += stepY;
            }

            return cells.ToList();
        }

        /// <summary>
        /// Получение лучей, проходящих через пиксель на проекции
        /// </summary>
        /// <param name="sinogram">
        /// The sinogram.
        /// </param>
        /// <param name="x">
        /// Номер пикселя по горизон
        /// </param>
        /// <param name="y">
        /// Вертикальная координата пикселя
        /// </param>
        /// <param name="projection">
        /// Номер проекции для которой проводится рассчёт
        /// </param>
        /// <returns>
        /// Список лучей, проходящих через пиксель
        /// </returns>
        private HashSet<int> FindRays(Sinogram sinogram, int x, int y, int projection)
        {
            var rays = new HashSet<int>();
            var angleInRad = projection * sinogram.StepAngle;
            var sin = Math.Sin(angleInRad);
            var cos = Math.Cos(angleInRad);
            var halfWidh = (float)sinogram.Width / 2;
            var xCoord = x - halfWidh;
            var yCoord = y - halfWidh;
            var center = halfWidh * sin;

            var bList = new List<double>
            {
                yCoord*cos + (xCoord+1)*sin + halfWidh, 
                (yCoord+1)*cos + xCoord*sin + halfWidh, 
                yCoord*cos + xCoord*sin + halfWidh, 
                (yCoord+1)*cos + (xCoord+1)*sin + halfWidh
            };

            var minB = bList.Min();
            var maxB = bList.Max();

            var minBpx = (int)Math.Round(minB);
            var maxBpx = (int)Math.Round(maxB);
            if (minBpx != maxBpx)
            {
                for (int i = minBpx; i < maxBpx; i++)
                {
                    if (i >= 0 && i < sinogram.Width)
                    {
                        rays.Add(i);
                    }
                }
            }

            return rays;
        }
    }
}
