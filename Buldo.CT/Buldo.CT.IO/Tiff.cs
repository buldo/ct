﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitMiracle.LibTiff.Classic;
using Buldo.CT.Common;

namespace Buldo.CT.IO
{
    public static class Tiff
    {
        public static Sinogram ReadSinoFromFile(string path, float degStep)
        {
            var sinogram = new List<List<float>>();

            using (var tiff = BitMiracle.LibTiff.Classic.Tiff.Open(path, "r"))
            {
                FieldValue[] res = tiff.GetField(TiffTag.IMAGELENGTH);
                int height = res[0].ToInt();

                res = tiff.GetField(TiffTag.IMAGEWIDTH);
                var width = res[0].ToInt();

                res = tiff.GetField(TiffTag.BITSPERSAMPLE);
                short bpp = res[0].ToShort();
                if (bpp != 32)
                    return null;

                res = tiff.GetField(TiffTag.SAMPLESPERPIXEL);
                short spp = res[0].ToShort();
                if (spp != 1)
                    return null;

                res = tiff.GetField(TiffTag.PHOTOMETRIC);
                Photometric photo = (Photometric)res[0].ToInt();
                if (photo != Photometric.MINISBLACK && photo != Photometric.MINISWHITE)
                    return null;

                int stride = tiff.ScanlineSize();
                var retList = new List<byte[]>();
                for (int i = 0; i < height; i++)
                {
                    byte[] buffer = new byte[stride];
                    tiff.ReadScanline(buffer, i);
                    retList.Add(buffer);
                }

                int pixelSize = stride / width;

                foreach (var line in retList)
                {
                    var converted = new List<float>();
                    for (var i = 0; i < width; i++)
                    {
                        converted.Add(BitConverter.ToSingle(line, i * pixelSize));
                    }

                    sinogram.Add(converted);
                }
            }

            return new Sinogram(sinogram, degStep);
        }

        public static void WriteArrayToFile(string path, float[,] imageArray)
        {
            using (var tiff = BitMiracle.LibTiff.Classic.Tiff.Open(path, "w"))
            {
                if (tiff == null)
                    return;
                var height = imageArray.GetLength(0);
                var width = imageArray.GetLength(1);

                tiff.SetField(TiffTag.IMAGEWIDTH, width);
                tiff.SetField(TiffTag.IMAGELENGTH, height);
                tiff.SetField(TiffTag.BITSPERSAMPLE, sizeof(float) * 8);
                tiff.SetField(TiffTag.SAMPLESPERPIXEL, 1);
                tiff.SetField(TiffTag.PHOTOMETRIC, Photometric.MINISBLACK);
                tiff.SetField(TiffTag.PLANARCONFIG, PlanarConfig.CONTIG);
                tiff.SetField(TiffTag.ROWSPERSTRIP, 1);
                tiff.SetField(TiffTag.SAMPLEFORMAT, SampleFormat.IEEEFP);
                
                for (int i = 0; i < height; i++)
                {
                    var buffer = new List<byte>();
                    for (int j = 0; j < width; j++)
                    {
                        buffer.AddRange(BitConverter.GetBytes(imageArray[i, j]));
                
                    }
                    tiff.WriteScanline(buffer.ToArray(), i);
                }

                tiff.FlushData();
                tiff.Close();
            }
        }
    }
}
